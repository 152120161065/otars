﻿using Ota.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ota.Entities.Concrete
{
    [Table("Tbl_Ses")]
    public class Ses : IEntity
    {

        
        [Column("Ses_testID")]
        public string testId { get; set; }
        [Key]
        [Column("Ses_rostime_mID")]
        public string rostime { get; set; }
        [Column("Ses_data")]
        public string ses_data { get; set; }
    }
}
