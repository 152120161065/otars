﻿using Ota.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ota.Entities.Concrete
{
    [Table("Tbl_Robot")]
    public class Robot : IEntity
    {
        
        [Column("Rbt_testID")]
        public string testId { get; set; }
        [Key]
        [Column("Rbt_rostime")]
        public string rostime { get; set; }
        [Column("Rbt_bas_acisi")]
        public double bas_acisi { get; set; }
        [Column("Rbt_konum_x")]
        public double konum_x { get; set; }
        [Column("Rbt_konum_y")]
        public double konum_y { get; set; }
        [Column("Rbt_c_konum_x")]
        public double ckonum_x { get; set; }
        [Column("Rbt_c_konum_y")]
        public double ckonum_y { get; set; }
        [Column("Rbt_plab_x")]
        public double plab_x { get; set; }
        [Column("Rbt_plab_y")]
        public double plab_y { get; set; }
        [Column("Rbt_ortam_sicakligi")]
        public double sicaklik { get; set; }

    }
}
