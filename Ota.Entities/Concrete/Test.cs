﻿using Ota.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ota.Entities.Concrete
{
    [Table("Tbl_Test")]
    public class Test : IEntity
    {
        [Key]
        [Column("Tst_id")]
        public string testId { get; set; }
        [Column("Tst_kullanici_id")]
        public int kullanici_id { get; set; }
        [Column("Tst_tarih")]
        public DateTime tarih { get; set; }
        [Column("Tst_tanim")]
        public string tanim { get; set; }
        [Column("Tst_delta_t")]
        public double delta_t { get; set; }
        [Column("Tst_frekans")]
        public double frekans { get; set; }
    }
}
