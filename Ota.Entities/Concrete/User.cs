﻿using Ota.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ota.Entities.Concrete
{
    [Table("Tbl_User")]
    public class User : IEntity
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        [Column("mail")]
        public string mail { get; set; }
        [Column("sifre")]
        public string sifre { get; set; }
        [Column("isim")]
        public string isim { get; set; }
        [Column("soyisim")]
        public string soyisim { get; set; }
        [Column("dogum_tarihi")]
        public DateTime Kl_dogum_tarihi { get; set; }
    }
}
