﻿using Ota.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ota.Entities.Concrete
{
    [Table("Tbl_Motor")]
    public class Motor : IEntity
    {
        [Column("Mtr_testID")]
        public string testId { get; set; }
        [Key]
        [Column("Mtr_rostime_mID")]
        public string rostime { get; set; }
        [Column("Mtr_motor_cekim_akimi")]
        public double cekim_akimi { get; set; }
        [Column("Mtr_teker_hizi")]
        public double teker_hizi { get; set; }
        [Column("Mtr_guc")]
        public double guc { get; set; }
        [Column("Mtr_gerilim")]
        public double gerilim { get; set; }
        [Column("Mtr_sicaklik")]
        public double sicaklik { get; set; }
        [Column("Mtr_nem")]
        public double nem { get; set; }
    }
}
