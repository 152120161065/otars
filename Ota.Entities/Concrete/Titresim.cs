﻿using Ota.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ota.Entities.Concrete
{
    [Table("Tbl_Titresim")]
    public class Titresim : IEntity
    {
        [Column("Ttr_testID")]
        public string testId { get; set; }
        [Key]
        [Column("Ttr_rostime_mID")]
        public string rostime { get; set; }
        [Column("Ttr_uc_eksen_ivme_x")]
        public string ivme_x { get; set; }
        [Column("Ttr_uc_eksen_ivme_y")]
        public string ivme_y { get; set; }
        [Column("Ttr_uc_eksen_ivme_z")]
        public string ivme_z { get; set; }
    }
}
