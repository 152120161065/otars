﻿using Ota.Entities.Concrete;
using Ota.Service.Abstract;
using Ota.Service.DependencyManager;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using LumenWorks.Framework.IO.Csv;
using System.Windows.Forms.DataVisualization.Charting;
using System.Text;
using System.Threading;
using System.Drawing;

namespace Ota.UI
{
    public partial class Main_Form : Form
    {

        private Random random = new Random();
        private IMotorService motorService;
        private IRobotService robotService;
        private ISesService sesService;
        private ITitresimService titService;
        private ITestService testService;
        public static string selectedTest;
        public Main_Form()
        {
            InitializeComponent();
            motorService = InstanceFactory.GetInstance<IMotorService>();
            robotService = InstanceFactory.GetInstance<IRobotService>();
            sesService = InstanceFactory.GetInstance<ISesService>();
            titService = InstanceFactory.GetInstance<ITitresimService>();
            testService = InstanceFactory.GetInstance<ITestService>();
        }
        private void Main_Form_Load(object sender, EventArgs e)
        {
            chVeri.Visible = false;
            try
            {
                lblUser.Text = String.Format("{0} {1}", Login_Form.userName, Login_Form.lastName).ToUpper();
                cmbTestList.Items.AddRange(testService.GetAll().Select(p => p.testId).ToArray());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        public Robot funcRobot(string dirName, string[] values)
        {
            Robot robot = new Robot();
            robot.testId = dirName;
            robot.rostime = values[0];
            if (values[1] != "None")
            {
                robot.bas_acisi = Convert.ToDouble(values[1], System.Globalization.CultureInfo.InvariantCulture);
                robot.sicaklik = Convert.ToDouble(values[2], System.Globalization.CultureInfo.InvariantCulture);
                robot.konum_x = Convert.ToDouble(values[3], System.Globalization.CultureInfo.InvariantCulture);
                robot.konum_y = Convert.ToDouble(values[4], System.Globalization.CultureInfo.InvariantCulture);
                robot.ckonum_x = Convert.ToDouble(values[5], System.Globalization.CultureInfo.InvariantCulture);
                robot.ckonum_y = Convert.ToDouble(values[6], System.Globalization.CultureInfo.InvariantCulture);
                robot.plab_x = Convert.ToDouble(values[7], System.Globalization.CultureInfo.InvariantCulture);
                robot.plab_y = Convert.ToDouble(values[8], System.Globalization.CultureInfo.InvariantCulture);
            }
            return robot;

        }
        public Tuple<Motor, Motor> funcMotor(string dirName, string[] values)
        {
            Motor motor1 = new Motor();
            Motor motor2 = new Motor();
            motor1.testId = dirName;
            motor2.testId = dirName;
            motor1.rostime = values[0] + "_1";
            motor2.rostime = values[0] + "_2";

            try
            {
                motor1.cekim_akimi = Convert.ToDouble(values[9], System.Globalization.CultureInfo.InvariantCulture);
                motor2.cekim_akimi = Convert.ToDouble(values[15], System.Globalization.CultureInfo.InvariantCulture);
                motor1.teker_hizi = Convert.ToDouble(values[10], System.Globalization.CultureInfo.InvariantCulture);
                motor2.teker_hizi = Convert.ToDouble(values[16], System.Globalization.CultureInfo.InvariantCulture);
                motor1.guc = Convert.ToDouble(values[11], System.Globalization.CultureInfo.InvariantCulture);
                motor2.guc = Convert.ToDouble(values[17], System.Globalization.CultureInfo.InvariantCulture);
                motor1.gerilim = Convert.ToDouble(values[12], System.Globalization.CultureInfo.InvariantCulture);
                motor2.gerilim = Convert.ToDouble(values[18], System.Globalization.CultureInfo.InvariantCulture);
                motor1.sicaklik = Convert.ToDouble(values[13], System.Globalization.CultureInfo.InvariantCulture);
                motor2.sicaklik = Convert.ToDouble(values[19], System.Globalization.CultureInfo.InvariantCulture);
                motor1.nem = Convert.ToDouble(values[14], System.Globalization.CultureInfo.InvariantCulture);
                motor2.nem = Convert.ToDouble(values[20], System.Globalization.CultureInfo.InvariantCulture);
            }
            catch (System.FormatException)
            {

            }

            return Tuple.Create(motor1, motor2);
        }
        public List<Ses> funcSes(string dirName, string fileName, int motorNo)
        {
            List<Ses> sesList = new List<Ses>();
            var csvTable = new DataTable();
            using (var csvReader = new CsvReader(new StreamReader(System.IO.File.OpenRead(fileName)), true, delimiter: ';'))
            {
                csvTable.Load(csvReader);
            }
            foreach (DataColumn col in csvTable.Columns)
            {
                Ses ses = new Ses();
                string[] values = csvTable.AsEnumerable().Select(s => s.Field<string>(col)).ToArray<string>();
                ses.rostime = col.ToString() + "_" + motorNo.ToString();
                ses.ses_data = String.Join(",", values.ToArray());
                ses.testId = dirName;
                sesList.Add(ses);
            }
            return sesList;
        }
        public List<Titresim> funcTitresim(string dirName, string fileName, int motorNo)
        {
            List<Titresim> titList = new List<Titresim>();
            var csvTable = new DataTable();
            using (var csvReader = new CsvReader(new StreamReader(System.IO.File.OpenRead(fileName)), true, delimiter: ';'))
            {
                csvTable.Load(csvReader);
            }
            string x = "", y = "", z = "";
            foreach (DataRow col in csvTable.Rows)
            {
                Titresim tit = new Titresim();
                var values = col.ItemArray;
                if (values[1] is "x")
                {
                    x = String.Join(",", values.Skip(2));
                }
                else if (values[1] is "y")
                {
                    y = String.Join(",", values.Skip(2));
                }
                else if (values[1] is "z")
                {
                    z = String.Join(",", values.Skip(2));
                    tit.rostime = values[0] + "_" + motorNo.ToString();
                    tit.testId = dirName;
                    tit.ivme_x = x;
                    tit.ivme_y = y;
                    tit.ivme_z = z;
                    titList.Add(tit);
                }

            }
            return titList;
        }
        public void readCSV(string[] files, string dirName, string deltaT, string frekans)
        {
            List<Motor> motorlist = new List<Motor>();
            List<Robot> robotlist = new List<Robot>();
            List<Ses> ses1List = new List<Ses>();
            List<Ses> ses2List = new List<Ses>();
            List<Titresim> tit1List = new List<Titresim>();
            List<Titresim> tit2List = new List<Titresim>();
            Test test = new Test();
            test.testId = dirName;
            test.kullanici_id = Login_Form.userId;
            test.frekans = Double.Parse(frekans);
            test.delta_t = Double.Parse(deltaT);
            test.tarih = DateTime.Parse(dirName.Split('_')[0]);
            test.tanim = "Null";

            foreach (var i in files)
            {
                string fileName = Path.GetFileName(i);

                switch (fileName)
                {
                    case "main.csv":
                        using (var reader = new StreamReader(i))
                        {
                            while (!reader.EndOfStream)
                            {
                                var line = reader.ReadLine();
                                var values = line.Split(';');
                                if (values[0] == "ROS Time") continue;
                                var robot = funcRobot(dirName, values);
                                var motors = funcMotor(dirName, values);
                                motorlist.Add(motors.Item1);
                                motorlist.Add(motors.Item2);
                                robotlist.Add(robot);
                            }
                        }
                        break;
                    case "motor1_ses.csv":
                        ses1List = funcSes(dirName, i, 1);
                        break;
                    case "motor2_ses.csv":
                        ses2List = funcSes(dirName, i, 2);
                        break;
                    case "motor1_vib.csv":
                        tit1List = funcTitresim(dirName, i, 1);
                        break;
                    case "motor2_vib.csv":
                        tit2List = funcTitresim(dirName, i, 2);
                        break;
                    default:

                        break;
                }
            }
            testService.addTest(test);
            motorService.addMotor(motorlist);
            robotService.addRobot(robotlist);
            sesService.addSes(ses1List);
            sesService.addSes(ses2List);
            titService.addTitresim(tit1List);
            titService.addTitresim(tit2List);
        }
        private void BindTitresimVeriToChart(char titNo, char location)
        {
            try
            {
                List<Titresim> titList = titService.findTitresimByTestId(cmbTestList.SelectedItem.ToString());
                List<Titresim> titByNumber = titList.FindAll(p => p.rostime.Last().Equals(titNo));
                List<Double> titDataList = new List<Double>();
                foreach (var item in titByNumber)
                {
                    List<String> titDataCollection = new List<string>();
                    switch (location)
                    {
                        case 'X': titDataCollection = item.ivme_x.Split(',').ToList(); break;
                        case 'Y': titDataCollection = item.ivme_y.Split(',').ToList(); break;
                        case 'Z': titDataCollection = item.ivme_z.Split(',').ToList(); break;
                    }
                    foreach (var titData in titDataCollection)
                    {
                       
                       titDataList.Add(Double.Parse(titData, System.Globalization.CultureInfo.InvariantCulture));
                        
                    }
                }
                Series sesSeries = new Series();
                sesSeries.Points.DataBindY(titDataList);
                chVeri.Series.Add(sesSeries);
                chVeri.Series[chVeri.Series.Count - 1].ChartType = SeriesChartType.Line;
                chVeri.Series[chVeri.Series.Count - 1].Name = "Titresim " + titNo.ToString() + "_" + location;
                chVeri.Series[chVeri.Series.Count - 1].Color = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void BindSesVeriToChart(char sesNo)
        {
            try
            {
                List<Ses> sesList = sesService.findSesByTestId(cmbTestList.SelectedItem.ToString());
                List<Ses> sesListByNumber = sesList.FindAll(p => p.rostime.Last().Equals(sesNo));
                List<int> sesDataList = new List<int>();
                foreach (var item in sesListByNumber)
                {
                    List<String> sesDataCollection = item.ses_data.Split(',').ToList();
                    foreach (var sesData in sesDataCollection)
                    {
                        if (sesData != "")
                        {
                            sesDataList.Add(Int32.Parse(sesData));
                        }
                        else
                        {
                            sesDataList.Add(0);
                        }
                    }
                }
                Series sesSeries = new Series();
                sesSeries.Points.DataBindY(sesDataList);
                chVeri.Series.Add(sesSeries);
                chVeri.Series[chVeri.Series.Count - 1].ChartType = SeriesChartType.Line;
                chVeri.Series[chVeri.Series.Count - 1].Name = "Ses " + sesNo.ToString();
                chVeri.Series[chVeri.Series.Count - 1].Color = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BindMotorVeriToChart(Char motorNo, String fieldName)
        {
            try
            {
                List<Motor> motorList = motorService.findMotorByTestId(cmbTestList.SelectedItem.ToString());
                List<Motor> motorListByNumber = motorList.FindAll(p => p.rostime.Last().Equals(motorNo));
                Series motorSeries = new Series();
                String seriesName = String.Empty;
                switch (fieldName)
                {
                    case "Teker Hizi":
                        motorSeries.Points.DataBindY(motorListByNumber.Select(p => p.teker_hizi).ToList());
                        seriesName = "Teker Hizi";
                        break;
                    case "Guc":
                        motorSeries.Points.DataBindY(motorListByNumber.Select(p => p.guc).ToList());
                        seriesName = "Guc";
                        break;
                    case "Gerilim":
                        motorSeries.Points.DataBindY(motorListByNumber.Select(p => p.gerilim).ToList());
                        seriesName = "Gerilim";
                        break;
                    case "Sicaklik":
                        motorSeries.Points.DataBindY(motorListByNumber.Select(p => p.sicaklik).ToList());
                        seriesName = "Sicaklik";
                        break;
                    case "Nem":
                        motorSeries.Points.DataBindY(motorListByNumber.Select(p => p.nem).ToList());
                        seriesName = "Nem";
                        break;
                    case "Motor Cekim":
                        motorSeries.Points.DataBindY(motorListByNumber.Select(p => p.cekim_akimi).ToList());
                        seriesName = "Motor Cekim";
                        break;
                }
                chVeri.Series.Add(motorSeries);
                chVeri.Series[chVeri.Series.Count - 1].ChartType = SeriesChartType.Line;
                chVeri.Series[chVeri.Series.Count - 1].Name = seriesName + motorNo.ToString();
                chVeri.Series[chVeri.Series.Count - 1].Color = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void BindRobotKonumToChart(String dataType)
        {
            try
            {
                List<Robot> robotList = robotService.findRobotByTestId(cmbTestList.SelectedItem.ToString());
                Series robotSeries = new Series();
                String seriesName = String.Empty;
                switch (dataType)
                {
                    case "Konum":
                        robotSeries.Points.DataBindXY(robotList.Select(p => p.konum_x).ToList(), robotList.Select(p => p.konum_y).ToList());
                        seriesName = "Konum";
                        break;
                    case "C_Konum":
                        robotSeries.Points.DataBindXY(robotList.Select(p => p.ckonum_x).ToList(), robotList.Select(p => p.ckonum_y).ToList());
                        seriesName = "C_Konum";
                        break;
                    case "Plab":
                        robotSeries.Points.DataBindXY(robotList.Select(p => p.plab_x).ToList(), robotList.Select(p => p.plab_y).ToList());
                        seriesName = "Plab";
                        break;
                    case "Sicaklik":
                        robotSeries.Points.DataBindY(robotList.Select(p => p.sicaklik).ToList());
                        seriesName = "Sicaklik";
                        break;
                }
                chVeri.Series.Add(robotSeries);
                chVeri.Series[chVeri.Series.Count - 1].ChartType = dataType == "Sicaklik" ? SeriesChartType.Line : SeriesChartType.Point;
                chVeri.Series[chVeri.Series.Count - 1].Name = seriesName + " " + dataType;
                chVeri.Series[chVeri.Series.Count - 1].Color = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void btnGrafik_Click(object sender, EventArgs e)
        {
            if (cmbTestList.Text != String.Empty)
            {
                chVeri.Visible = true;
                btnClear.Visible = true;
                if (cmbGrafik.SelectedItem is null)
                {
                    MessageBox.Show("Lütfen geçerli bir grafik türü giriniz!");
                }
                switch (cmbGrafik.SelectedIndex)
                {
                    case 0: BindMotorVeriToChart('1', "Teker Hizi"); break;
                    case 1: BindMotorVeriToChart('2', "Teker Hizi"); break;
                    case 2: BindMotorVeriToChart('1', "Guc"); break;
                    case 3: BindMotorVeriToChart('2', "Guc"); break;
                    case 4: BindMotorVeriToChart('1', "Gerilim"); break;
                    case 5: BindMotorVeriToChart('2', "Gerilim"); break;
                    case 6: BindMotorVeriToChart('1', "Sicaklik"); break;
                    case 7: BindMotorVeriToChart('2', "Sicaklik"); break;
                    case 8: BindMotorVeriToChart('1', "Nem"); break;
                    case 9: BindMotorVeriToChart('2', "Nem"); break;
                    case 10: BindMotorVeriToChart('1', "Motor Cekim"); break;
                    case 11: BindMotorVeriToChart('2', "Motor Cekim"); break;
                    case 12: BindRobotKonumToChart("Konum"); break;
                    case 13: BindRobotKonumToChart("C_Konum"); break;
                    case 14: BindRobotKonumToChart("Plab"); break;
                    case 15: BindRobotKonumToChart("Sicaklik"); break;
                    case 16: BindSesVeriToChart('1'); break;
                    case 17: BindSesVeriToChart('2'); break;
                    case 18: BindTitresimVeriToChart('1', 'X'); break;
                    case 19: BindTitresimVeriToChart('1', 'Y'); break;
                    case 20: BindTitresimVeriToChart('1', 'Z'); break;
                    case 21: BindTitresimVeriToChart('2', 'X'); break;
                    case 22: BindTitresimVeriToChart('2', 'Y'); break;
                    case 23: BindTitresimVeriToChart('2', 'Z'); break;
                }
            }
            else
            {
                MessageBox.Show("Lütfen geçerli bir veri giriniz!");
            }
        }

        private void reloadData()
        {
            cmbTestList.Items.Clear();
            cmbTestList.Items.AddRange(testService.GetAll().Select(p => p.testId).ToArray());
        }
        private void Main_Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (MessageBox.Show("Emin misiniz?", "Çıkış", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
                    e.Cancel = true;
                else
                    Application.Exit();
            }
        }

        private void btnViewData_Click(object sender, EventArgs e)
        {
            Data_Form data = new Data_Form();
            data.ShowDialog();
        }

        private void cmbTestList_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedTest = cmbTestList.SelectedItem.ToString();
            chVeri.Series.Clear();
        }

        private void iceAktar_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.ShowDialog();
            new Thread(() =>
            {
                Invoke(new Action(() =>
                {
                    lblInfo.Visible = true;
                    pbProcess.Visible = true;
                    pbProcess.Style = ProgressBarStyle.Marquee;
                    pbProcess.MarqueeAnimationSpeed = 50;
                }));
                Thread.CurrentThread.IsBackground = true;
                string path = fbd.SelectedPath;
                if (!(path is ""))
                {
                    string dirName = Path.GetFileName(path);
                    string name = dirName.Split('_')[0] + "_" + dirName.Split('_')[1];
                    string frekans = dirName.Split('_')[2];
                    string deltaT = dirName.Split('_')[3];
                    string[] files = Directory.GetFiles(path);
                    readCSV(files, name, deltaT, frekans);
                    if (MessageBox.Show("Veri aktarımı tamamlandı.", "Başarılı", MessageBoxButtons.OK, MessageBoxIcon.Information) == DialogResult.OK)
                    {
                        Invoke(new Action(() =>
                        {
                            lblInfo.Visible = false;
                            pbProcess.Visible = false;
                            pbProcess.Style = ProgressBarStyle.Continuous;
                            pbProcess.MarqueeAnimationSpeed = 0;
                            reloadData();
                        }));
                    }

                }
            }).Start();
        }

        private void disaAktar_click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.ShowDialog();
            if (!(fbd.SelectedPath is ""))
            {
                if (cmbTestList.Text != String.Empty)
                {
                    var linesMain = new List<string>();
                    var linesSes1 = new List<string>();
                    var linesSes2 = new List<string>();
                    var linesTit1 = new List<string>();
                    var linesTit2 = new List<string>();


                    String baslik = "ROS Time;Robot Bas Acisi;Ortam Sicakligi;Konum x;Konum y;D_Konum x;D_Konum y;Plab Konum x;Plab Konum y;Motor1 Cekim Akimi;Motor1 Teker Hizi;Motor1 Guc;Motor1 Aku Gerilim;Motor1 Sicaklik;Motor1 Nem;Motor2 Cekim Akimi;Motor2 Teker Hizi;Motor2 Guc;Motor2 Aku Gerilim;Motor2 Sicaklik;Motor2 Nem;";
                    linesMain.Add(baslik);

                    List<Ses> sesList = sesService.findSesByTestId(cmbTestList.SelectedItem.ToString());
                    List<Ses> ses1List = sesList.FindAll(p => p.rostime.Last().Equals('1'));
                    List<Ses> ses2List = sesList.FindAll(p => p.rostime.Last().Equals('2'));

                    List<Titresim> titList = titService.findTitresimByTestId(cmbTestList.SelectedItem.ToString());
                    List<Titresim> tit1List = titList.FindAll(p => p.rostime.Last().Equals('1'));
                    List<Titresim> tit2List = titList.FindAll(p => p.rostime.Last().Equals('2'));

                    List<Motor> motorList = motorService.findMotorByTestId(cmbTestList.SelectedItem.ToString());
                    List<Motor> motor1List = motorList.FindAll(p => p.rostime.Last().Equals('1'));
                    List<Motor> motor2List = motorList.FindAll(p => p.rostime.Last().Equals('2'));
                    List<Robot> robotList = robotService.findRobotByTestId(cmbTestList.SelectedItem.ToString());
                    IEnumerable<Tuple<Robot, Motor, Motor>> mainList = robotList.Zip(motor1List, (r, m1) => new { r, m1 }).Zip(motor2List, (z1, m2) => Tuple.Create(z1.r, z1.m1, m2));

                    foreach (var (r, m1, m2) in mainList)
                    {
                        String line = String.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13};{14};{15};{16};{17};{18};{19};{20};", r.rostime, r.bas_acisi, r.sicaklik, r.konum_x, r.konum_y, r.ckonum_x, r.ckonum_y, r.plab_x, r.plab_y, m1.cekim_akimi, m1.teker_hizi, m1.guc, m1.gerilim, m1.sicaklik, m1.nem, m2.cekim_akimi, m2.teker_hizi, m2.guc, m2.gerilim, m2.sicaklik, m2.nem);
                        linesMain.Add(line);
                    }
                    foreach (var (s1, s2) in ses1List.Zip(ses2List, (s1, s2) => (s1, s2)))
                    {
                        if (s1.ses_data[0] is ',')
                        {
                            linesSes1.Add(s1.rostime.Split('_')[0]);
                            linesSes2.Add(s2.rostime.Split('_')[0]);
                            continue;
                        }
                        String col1 = String.Format("{0};{1}", s1.rostime.Split('_')[0], s1.ses_data.Replace(',', ';'));
                        String col2 = String.Format("{0};{1}", s2.rostime.Split('_')[0], s2.ses_data.Replace(',', ';'));
                        linesSes1.Add(col1);
                        linesSes2.Add(col2);
                    }

                    foreach (var (t1, t2) in tit1List.Zip(tit2List, (t1, t2) => (t1, t2)))
                    {
                        String rowx1 = String.Format("{0};x;{1}", t1.rostime.Split('_')[0], t1.ivme_x.Replace(',', ';').Replace('.', ','));
                        String rowy1 = String.Format("{0};y;{1}", t1.rostime.Split('_')[0], t1.ivme_y.Replace(',', ';').Replace('.', ','));
                        String rowz1 = String.Format("{0};z;{1}", t1.rostime.Split('_')[0], t1.ivme_z.Replace(',', ';').Replace('.', ','));
                        String rowx2 = String.Format("{0};x;{1}", t2.rostime.Split('_')[0], t2.ivme_x.Replace(',', ';').Replace('.', ','));
                        String rowy2 = String.Format("{0};y;{1}", t2.rostime.Split('_')[0], t2.ivme_y.Replace(',', ';').Replace('.', ','));
                        String rowz2 = String.Format("{0};z;{1}", t2.rostime.Split('_')[0], t2.ivme_z.Replace(',', ';').Replace('.', ','));

                        linesTit1.Add(rowx1); linesTit1.Add(rowy1); linesTit1.Add(rowz1);
                        linesTit2.Add(rowx2); linesTit2.Add(rowy2); linesTit2.Add(rowz2);
                    }

                    string dirName = robotList[0].testId;
                    Directory.CreateDirectory(Path.Combine(fbd.SelectedPath, dirName));
                    File.WriteAllLines(Path.Combine(fbd.SelectedPath, dirName, "main.csv"), linesMain.ToArray());
                    File.WriteAllLines(Path.Combine(fbd.SelectedPath, dirName, "motor1_ses.csv"), linesSes1.ToArray());
                    File.WriteAllLines(Path.Combine(fbd.SelectedPath, dirName, "motor2_ses.csv"), linesSes2.ToArray());
                    File.WriteAllLines(Path.Combine(fbd.SelectedPath, dirName, "motor1_titresim.csv"), linesTit1.ToArray());
                    File.WriteAllLines(Path.Combine(fbd.SelectedPath, dirName, "motor2_titresim.csv"), linesTit2.ToArray());
                    MessageBox.Show("Aktarım Tamamlandı.", "Tamamlandı", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Dışa aktarılacak test verisi seçilmedi.", "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
        }
        private void ekranGoruntusuAl_Click(object sender, EventArgs e)
        {
            if (chVeri.Series.Count > 0)
            {
                try
                {
                    Bitmap bmpScreenshot = new Bitmap(chVeri.Width, chVeri.Height);
                    chVeri.DrawToBitmap(bmpScreenshot, new Rectangle(0, 0, bmpScreenshot.Width, bmpScreenshot.Height));
                    string fileName = string.Format(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                              @"\Screenshot" + "_" +
                              DateTime.Now.ToString("(dd_MMMM_hh_mm_ss)") + ".png");
                    bmpScreenshot.Save(fileName);
                    MessageBox.Show("Ekran görüntüsü belgeler altına kaydedildi.");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Herhangi bir veri seçmediniz.", "Başarısız", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            chVeri.Series.Clear();
        }
    }
}
