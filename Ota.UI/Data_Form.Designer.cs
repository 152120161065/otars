﻿namespace Ota.UI
{
    partial class Data_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Data_Form));
            this.tabData = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.gridGenel = new System.Windows.Forms.DataGridView();
            this.testID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rostime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bas_aci = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ortam_sicakligi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.konum_x = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.konum_y = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d_konum_x = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.d_konum_y = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.plab_x = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.plab_y = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m1_cekim = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m1_teker_hizi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m1_guc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m1_gerilim = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m1_sicaklik = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m1_nem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m2_cekim = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m2_teker_hizi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m2_guc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m2_gerilim = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m2_sicaklik = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.m2_nem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.gridSes1 = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.gridSes2 = new System.Windows.Forms.DataGridView();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.gridTitresim1 = new System.Windows.Forms.DataGridView();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.gridTitresim2 = new System.Windows.Forms.DataGridView();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.tabData.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridGenel)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSes1)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSes2)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridTitresim1)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridTitresim2)).BeginInit();
            this.SuspendLayout();
            // 
            // tabData
            // 
            this.tabData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabData.Controls.Add(this.tabPage1);
            this.tabData.Controls.Add(this.tabPage2);
            this.tabData.Controls.Add(this.tabPage3);
            this.tabData.Controls.Add(this.tabPage4);
            this.tabData.Controls.Add(this.tabPage5);
            this.tabData.Location = new System.Drawing.Point(12, 34);
            this.tabData.Name = "tabData";
            this.tabData.SelectedIndex = 0;
            this.tabData.Size = new System.Drawing.Size(1220, 620);
            this.tabData.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.gridGenel);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1212, 594);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Genel";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // gridGenel
            // 
            this.gridGenel.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridGenel.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(49)))), ((int)(((byte)(60)))));
            this.gridGenel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridGenel.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.testID,
            this.rostime,
            this.bas_aci,
            this.ortam_sicakligi,
            this.konum_x,
            this.konum_y,
            this.d_konum_x,
            this.d_konum_y,
            this.plab_x,
            this.plab_y,
            this.m1_cekim,
            this.m1_teker_hizi,
            this.m1_guc,
            this.m1_gerilim,
            this.m1_sicaklik,
            this.m1_nem,
            this.m2_cekim,
            this.m2_teker_hizi,
            this.m2_guc,
            this.m2_gerilim,
            this.m2_sicaklik,
            this.m2_nem});
            this.gridGenel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridGenel.Location = new System.Drawing.Point(3, 3);
            this.gridGenel.MultiSelect = false;
            this.gridGenel.Name = "gridGenel";
            this.gridGenel.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridGenel.Size = new System.Drawing.Size(1206, 588);
            this.gridGenel.TabIndex = 0;
            this.gridGenel.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridGenel_CellValueChanged);
            // 
            // testID
            // 
            this.testID.HeaderText = "testID";
            this.testID.Name = "testID";
            // 
            // rostime
            // 
            this.rostime.HeaderText = "rostime";
            this.rostime.Name = "rostime";
            // 
            // bas_aci
            // 
            this.bas_aci.HeaderText = "Robot Baş Açısı";
            this.bas_aci.Name = "bas_aci";
            // 
            // ortam_sicakligi
            // 
            this.ortam_sicakligi.HeaderText = "Ortam Sıcaklığı";
            this.ortam_sicakligi.Name = "ortam_sicakligi";
            // 
            // konum_x
            // 
            this.konum_x.HeaderText = "Konum_X";
            this.konum_x.Name = "konum_x";
            // 
            // konum_y
            // 
            this.konum_y.HeaderText = "Konum_Y";
            this.konum_y.Name = "konum_y";
            // 
            // d_konum_x
            // 
            this.d_konum_x.HeaderText = "D_Konum_X";
            this.d_konum_x.Name = "d_konum_x";
            // 
            // d_konum_y
            // 
            this.d_konum_y.HeaderText = "D_Konum_Y";
            this.d_konum_y.Name = "d_konum_y";
            // 
            // plab_x
            // 
            this.plab_x.HeaderText = "Plab Konum_X";
            this.plab_x.Name = "plab_x";
            // 
            // plab_y
            // 
            this.plab_y.HeaderText = "Plab Konum_Y";
            this.plab_y.Name = "plab_y";
            // 
            // m1_cekim
            // 
            this.m1_cekim.HeaderText = "Motor1 Çekim Akımı";
            this.m1_cekim.Name = "m1_cekim";
            // 
            // m1_teker_hizi
            // 
            this.m1_teker_hizi.HeaderText = "Motor1 Teker Hızı";
            this.m1_teker_hizi.Name = "m1_teker_hizi";
            // 
            // m1_guc
            // 
            this.m1_guc.HeaderText = "Motor1 Güç";
            this.m1_guc.Name = "m1_guc";
            // 
            // m1_gerilim
            // 
            this.m1_gerilim.HeaderText = "Motor1 Gerilim";
            this.m1_gerilim.Name = "m1_gerilim";
            // 
            // m1_sicaklik
            // 
            this.m1_sicaklik.HeaderText = "Motor1 Sıcaklık";
            this.m1_sicaklik.Name = "m1_sicaklik";
            // 
            // m1_nem
            // 
            this.m1_nem.HeaderText = "Motor1 Nem";
            this.m1_nem.Name = "m1_nem";
            // 
            // m2_cekim
            // 
            this.m2_cekim.HeaderText = "Motor2 Çekim Akımı";
            this.m2_cekim.Name = "m2_cekim";
            // 
            // m2_teker_hizi
            // 
            this.m2_teker_hizi.HeaderText = "Motor2 Teker Hızı";
            this.m2_teker_hizi.Name = "m2_teker_hizi";
            // 
            // m2_guc
            // 
            this.m2_guc.HeaderText = "Motor2 Güç";
            this.m2_guc.Name = "m2_guc";
            // 
            // m2_gerilim
            // 
            this.m2_gerilim.HeaderText = "Motor2 Gerilim";
            this.m2_gerilim.Name = "m2_gerilim";
            // 
            // m2_sicaklik
            // 
            this.m2_sicaklik.HeaderText = "Motor2 Sıcaklık";
            this.m2_sicaklik.Name = "m2_sicaklik";
            // 
            // m2_nem
            // 
            this.m2_nem.HeaderText = "Motor2 Nem";
            this.m2_nem.Name = "m2_nem";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.gridSes1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1212, 594);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Motor1 Ses";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // gridSes1
            // 
            this.gridSes1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridSes1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridSes1.DefaultCellStyle = dataGridViewCellStyle1;
            this.gridSes1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSes1.Location = new System.Drawing.Point(3, 3);
            this.gridSes1.MultiSelect = false;
            this.gridSes1.Name = "gridSes1";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            this.gridSes1.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.gridSes1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridSes1.Size = new System.Drawing.Size(1206, 588);
            this.gridSes1.TabIndex = 1;
            this.gridSes1.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridSes1_CellValueChanged);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.gridSes2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1212, 594);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Motor2 Ses";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // gridSes2
            // 
            this.gridSes2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridSes2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridSes2.DefaultCellStyle = dataGridViewCellStyle3;
            this.gridSes2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridSes2.Location = new System.Drawing.Point(3, 3);
            this.gridSes2.MultiSelect = false;
            this.gridSes2.Name = "gridSes2";
            this.gridSes2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridSes2.Size = new System.Drawing.Size(1206, 588);
            this.gridSes2.TabIndex = 1;
            this.gridSes2.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridSes2_CellValueChanged);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.gridTitresim1);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1212, 594);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Motor1 Titreşim";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // gridTitresim1
            // 
            this.gridTitresim1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridTitresim1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridTitresim1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridTitresim1.Location = new System.Drawing.Point(3, 3);
            this.gridTitresim1.MultiSelect = false;
            this.gridTitresim1.Name = "gridTitresim1";
            this.gridTitresim1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridTitresim1.Size = new System.Drawing.Size(1206, 588);
            this.gridTitresim1.TabIndex = 1;
            this.gridTitresim1.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridTitresim1_CellValueChanged);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.gridTitresim2);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1212, 594);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Motor2 Titreşim";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // gridTitresim2
            // 
            this.gridTitresim2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gridTitresim2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridTitresim2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridTitresim2.Location = new System.Drawing.Point(3, 3);
            this.gridTitresim2.MultiSelect = false;
            this.gridTitresim2.Name = "gridTitresim2";
            this.gridTitresim2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridTitresim2.Size = new System.Drawing.Size(1206, 588);
            this.gridTitresim2.TabIndex = 1;
            this.gridTitresim2.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.gridTitresim2_CellValueChanged);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.Location = new System.Drawing.Point(1119, 5);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(101, 32);
            this.btnUpdate.TabIndex = 2;
            this.btnUpdate.Text = "Güncelle";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Location = new System.Drawing.Point(1012, 5);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(101, 32);
            this.btnDelete.TabIndex = 3;
            this.btnDelete.Text = "Sil";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // Data_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(49)))), ((int)(((byte)(60)))));
            this.ClientSize = new System.Drawing.Size(1234, 658);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.tabData);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Data_Form";
            this.Text = "Ota Veri Raporlama Sistemi";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Data_Form_FormClosing);
            this.Load += new System.EventHandler(this.Data_Form_Load);
            this.tabData.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridGenel)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSes1)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridSes2)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridTitresim1)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridTitresim2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabControl tabData;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.DataGridView gridSes1;
        private System.Windows.Forms.DataGridView gridSes2;
        private System.Windows.Forms.DataGridView gridTitresim1;
        private System.Windows.Forms.DataGridView gridTitresim2;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.DataGridView gridGenel;
        private System.Windows.Forms.DataGridViewTextBoxColumn testID;
        private System.Windows.Forms.DataGridViewTextBoxColumn rostime;
        private System.Windows.Forms.DataGridViewTextBoxColumn bas_aci;
        private System.Windows.Forms.DataGridViewTextBoxColumn ortam_sicakligi;
        private System.Windows.Forms.DataGridViewTextBoxColumn konum_x;
        private System.Windows.Forms.DataGridViewTextBoxColumn konum_y;
        private System.Windows.Forms.DataGridViewTextBoxColumn d_konum_x;
        private System.Windows.Forms.DataGridViewTextBoxColumn d_konum_y;
        private System.Windows.Forms.DataGridViewTextBoxColumn plab_x;
        private System.Windows.Forms.DataGridViewTextBoxColumn plab_y;
        private System.Windows.Forms.DataGridViewTextBoxColumn m1_cekim;
        private System.Windows.Forms.DataGridViewTextBoxColumn m1_teker_hizi;
        private System.Windows.Forms.DataGridViewTextBoxColumn m1_guc;
        private System.Windows.Forms.DataGridViewTextBoxColumn m1_gerilim;
        private System.Windows.Forms.DataGridViewTextBoxColumn m1_sicaklik;
        private System.Windows.Forms.DataGridViewTextBoxColumn m1_nem;
        private System.Windows.Forms.DataGridViewTextBoxColumn m2_cekim;
        private System.Windows.Forms.DataGridViewTextBoxColumn m2_teker_hizi;
        private System.Windows.Forms.DataGridViewTextBoxColumn m2_guc;
        private System.Windows.Forms.DataGridViewTextBoxColumn m2_gerilim;
        private System.Windows.Forms.DataGridViewTextBoxColumn m2_sicaklik;
        private System.Windows.Forms.DataGridViewTextBoxColumn m2_nem;
        private System.Windows.Forms.Button btnDelete;
    }
}