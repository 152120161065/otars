﻿namespace Ota.UI
{
    partial class Main_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main_Form));
            this.chVeri = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.verileriAktarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.içeAktarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dışaAktarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ekranGörüntüsüAlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pbProcess = new System.Windows.Forms.ProgressBar();
            this.lblInfo = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.lblUser = new System.Windows.Forms.Label();
            this.btnViewData = new System.Windows.Forms.Button();
            this.lblTest = new System.Windows.Forms.Label();
            this.btnGrafik = new System.Windows.Forms.Button();
            this.cmbTestList = new System.Windows.Forms.ComboBox();
            this.cmbGrafik = new System.Windows.Forms.ComboBox();
            this.lblData = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.chVeri)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // chVeri
            // 
            this.chVeri.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chVeri.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(49)))), ((int)(((byte)(60)))));
            this.chVeri.BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.HorizontalCenter;
            this.chVeri.BackSecondaryColor = System.Drawing.Color.DarkGray;
            this.chVeri.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            this.chVeri.BorderSkin.PageColor = System.Drawing.Color.Transparent;
            chartArea1.Name = "ChartArea1";
            this.chVeri.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chVeri.Legends.Add(legend1);
            this.chVeri.Location = new System.Drawing.Point(304, 68);
            this.chVeri.Margin = new System.Windows.Forms.Padding(2);
            this.chVeri.Name = "chVeri";
            this.chVeri.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            this.chVeri.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(177)))), ((int)(((byte)(136)))))};
            this.chVeri.Size = new System.Drawing.Size(790, 548);
            this.chVeri.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(177)))), ((int)(((byte)(136)))));
            this.label1.Location = new System.Drawing.Point(513, 32);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(339, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "OTONOM TAŞIYICI ARAÇ SİSTEMİ";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.verileriAktarToolStripMenuItem,
            this.ekranGörüntüsüAlToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1188, 24);
            this.menuStrip1.TabIndex = 12;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // verileriAktarToolStripMenuItem
            // 
            this.verileriAktarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.içeAktarToolStripMenuItem,
            this.dışaAktarToolStripMenuItem});
            this.verileriAktarToolStripMenuItem.Name = "verileriAktarToolStripMenuItem";
            this.verileriAktarToolStripMenuItem.Size = new System.Drawing.Size(85, 20);
            this.verileriAktarToolStripMenuItem.Text = "Verileri Aktar";
            // 
            // içeAktarToolStripMenuItem
            // 
            this.içeAktarToolStripMenuItem.Name = "içeAktarToolStripMenuItem";
            this.içeAktarToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.içeAktarToolStripMenuItem.Text = "İçe Aktar";
            this.içeAktarToolStripMenuItem.Click += new System.EventHandler(this.iceAktar_Click);
            // 
            // dışaAktarToolStripMenuItem
            // 
            this.dışaAktarToolStripMenuItem.Name = "dışaAktarToolStripMenuItem";
            this.dışaAktarToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.dışaAktarToolStripMenuItem.Text = "Dışa Aktar";
            this.dışaAktarToolStripMenuItem.Click += new System.EventHandler(this.disaAktar_click);
            // 
            // ekranGörüntüsüAlToolStripMenuItem
            // 
            this.ekranGörüntüsüAlToolStripMenuItem.Name = "ekranGörüntüsüAlToolStripMenuItem";
            this.ekranGörüntüsüAlToolStripMenuItem.Size = new System.Drawing.Size(121, 20);
            this.ekranGörüntüsüAlToolStripMenuItem.Text = "Ekran Görüntüsü Al";
            this.ekranGörüntüsüAlToolStripMenuItem.Click += new System.EventHandler(this.ekranGoruntusuAl_Click);
            // 
            // pbProcess
            // 
            this.pbProcess.Location = new System.Drawing.Point(304, 631);
            this.pbProcess.Name = "pbProcess";
            this.pbProcess.Size = new System.Drawing.Size(790, 13);
            this.pbProcess.TabIndex = 13;
            this.pbProcess.Visible = false;
            // 
            // lblInfo
            // 
            this.lblInfo.AutoSize = true;
            this.lblInfo.BackColor = System.Drawing.SystemColors.ControlLight;
            this.lblInfo.Location = new System.Drawing.Point(662, 631);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(93, 13);
            this.lblInfo.TabIndex = 14;
            this.lblInfo.Text = "Veriler Aktarılıyor...";
            this.lblInfo.Visible = false;
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(49)))), ((int)(((byte)(60)))));
            this.btnClear.BackgroundImage = global::Ota.UI.Properties.Resources.refresh_icon_white_20;
            this.btnClear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Location = new System.Drawing.Point(1063, 585);
            this.btnClear.Margin = new System.Windows.Forms.Padding(2);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(30, 30);
            this.btnClear.TabIndex = 6;
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Visible = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel2.BackgroundImage")));
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.btnViewData);
            this.panel2.Controls.Add(this.lblTest);
            this.panel2.Controls.Add(this.btnGrafik);
            this.panel2.Controls.Add(this.cmbTestList);
            this.panel2.Controls.Add(this.cmbGrafik);
            this.panel2.Controls.Add(this.lblData);
            this.panel2.Location = new System.Drawing.Point(9, 66);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(267, 551);
            this.panel2.TabIndex = 2;
            // 
            // panel3
            // 
            this.panel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel3.BackgroundImage")));
            this.panel3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel3.Controls.Add(this.pictureBox4);
            this.panel3.Controls.Add(this.lblUser);
            this.panel3.Location = new System.Drawing.Point(55, 14);
            this.panel3.Margin = new System.Windows.Forms.Padding(2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(158, 140);
            this.panel3.TabIndex = 10;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(21, 2);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(115, 98);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 0;
            this.pictureBox4.TabStop = false;
            // 
            // lblUser
            // 
            this.lblUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblUser.ForeColor = System.Drawing.Color.White;
            this.lblUser.Location = new System.Drawing.Point(0, 102);
            this.lblUser.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(156, 37);
            this.lblUser.TabIndex = 1;
            this.lblUser.Text = "Kullanıcı Adı";
            this.lblUser.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnViewData
            // 
            this.btnViewData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(49)))), ((int)(((byte)(60)))));
            this.btnViewData.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnViewData.ForeColor = System.Drawing.Color.White;
            this.btnViewData.Image = global::Ota.UI.Properties.Resources.statistics32;
            this.btnViewData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnViewData.Location = new System.Drawing.Point(43, 416);
            this.btnViewData.Margin = new System.Windows.Forms.Padding(2);
            this.btnViewData.Name = "btnViewData";
            this.btnViewData.Size = new System.Drawing.Size(182, 46);
            this.btnViewData.TabIndex = 9;
            this.btnViewData.Text = "Verileri Göster";
            this.btnViewData.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnViewData.UseVisualStyleBackColor = false;
            this.btnViewData.Click += new System.EventHandler(this.btnViewData_Click);
            // 
            // lblTest
            // 
            this.lblTest.AutoSize = true;
            this.lblTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblTest.ForeColor = System.Drawing.Color.Black;
            this.lblTest.Location = new System.Drawing.Point(40, 176);
            this.lblTest.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTest.Name = "lblTest";
            this.lblTest.Size = new System.Drawing.Size(87, 16);
            this.lblTest.TabIndex = 4;
            this.lblTest.Text = "Test Seçiniz :";
            // 
            // btnGrafik
            // 
            this.btnGrafik.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(49)))), ((int)(((byte)(60)))));
            this.btnGrafik.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnGrafik.ForeColor = System.Drawing.Color.White;
            this.btnGrafik.Image = global::Ota.UI.Properties.Resources.growth32;
            this.btnGrafik.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGrafik.Location = new System.Drawing.Point(43, 367);
            this.btnGrafik.Margin = new System.Windows.Forms.Padding(2);
            this.btnGrafik.Name = "btnGrafik";
            this.btnGrafik.Size = new System.Drawing.Size(182, 45);
            this.btnGrafik.TabIndex = 6;
            this.btnGrafik.Text = "Grafik Ekle";
            this.btnGrafik.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGrafik.UseVisualStyleBackColor = false;
            this.btnGrafik.Click += new System.EventHandler(this.btnGrafik_Click);
            // 
            // cmbTestList
            // 
            this.cmbTestList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(49)))), ((int)(((byte)(60)))));
            this.cmbTestList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTestList.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.cmbTestList.FormattingEnabled = true;
            this.cmbTestList.Location = new System.Drawing.Point(43, 194);
            this.cmbTestList.Margin = new System.Windows.Forms.Padding(2);
            this.cmbTestList.Name = "cmbTestList";
            this.cmbTestList.Size = new System.Drawing.Size(182, 21);
            this.cmbTestList.TabIndex = 2;
            this.cmbTestList.SelectedIndexChanged += new System.EventHandler(this.cmbTestList_SelectedIndexChanged);
            // 
            // cmbGrafik
            // 
            this.cmbGrafik.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(49)))), ((int)(((byte)(60)))));
            this.cmbGrafik.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGrafik.ForeColor = System.Drawing.Color.White;
            this.cmbGrafik.FormattingEnabled = true;
            this.cmbGrafik.Items.AddRange(new object[] {
            "Motor1-Teker Hızı",
            "Motor2-Teker Hızı",
            "Motor1-Guc",
            "Motor2-Guc",
            "Motor1-Gerilim",
            "Motor2-Gerilim",
            "Motor1-Sicaklik",
            "Motor2-Sicaklik",
            "Motor1-Nem",
            "Motor2-Nem",
            "Motor1-Cekim Akımı",
            "Motor2-Cekim Akımı",
            "Robot-Konum",
            "Robot-C_Konum",
            "Robot_PlabKonum",
            "Robot_Sicaklik",
            "Ses-1",
            "Ses-2",
            "Titresim 1-X",
            "Titresim 1-Y",
            "Titresim 1-Z",
            "Titresim 2-X",
            "Titresim 2-Y",
            "Titresim 2-Z"});
            this.cmbGrafik.Location = new System.Drawing.Point(43, 261);
            this.cmbGrafik.Margin = new System.Windows.Forms.Padding(2);
            this.cmbGrafik.Name = "cmbGrafik";
            this.cmbGrafik.Size = new System.Drawing.Size(182, 21);
            this.cmbGrafik.TabIndex = 3;
            // 
            // lblData
            // 
            this.lblData.AutoSize = true;
            this.lblData.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblData.Location = new System.Drawing.Point(40, 243);
            this.lblData.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblData.Name = "lblData";
            this.lblData.Size = new System.Drawing.Size(89, 16);
            this.lblData.TabIndex = 5;
            this.lblData.Text = "Data Seçiniz :";
            // 
            // Main_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(49)))), ((int)(((byte)(60)))));
            this.ClientSize = new System.Drawing.Size(1188, 655);
            this.Controls.Add(this.lblInfo);
            this.Controls.Add(this.pbProcess);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.chVeri);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Main_Form";
            this.Text = "Ota Veri Raporlama Sistemi";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_Form_FormClosing);
            this.Load += new System.EventHandler(this.Main_Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chVeri)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chVeri;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox cmbTestList;
        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label lblData;
        private System.Windows.Forms.Label lblTest;
        private System.Windows.Forms.ComboBox cmbGrafik;
        private System.Windows.Forms.Button btnGrafik;
        private System.Windows.Forms.Button btnViewData;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem verileriAktarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem içeAktarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dışaAktarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ekranGörüntüsüAlToolStripMenuItem;
        private System.Windows.Forms.ProgressBar pbProcess;
        private System.Windows.Forms.Label lblInfo;
        private System.Windows.Forms.Button btnClear;
    }
}