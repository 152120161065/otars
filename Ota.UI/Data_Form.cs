﻿using Ota.Entities.Concrete;
using Ota.Service.Abstract;
using Ota.Service.DependencyManager;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Design;
using System.Drawing.Imaging;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Ota.UI
{
    public partial class Data_Form : Form
    {
        List<Ses> sesList;
        BindingList<Ses> ses1List;
        BindingList<Ses> ses2List;

        List<Titresim> titList;
        BindingList<Titresim> tit1List;
        BindingList<Titresim> tit2List;

        List<Motor> motorList;
        BindingList<Motor> motor1List;
        BindingList<Motor> motor2List;
        BindingList<Robot> robotList;
        private IMotorService motorService;
        private IRobotService robotService;
        private ISesService sesService;
        private ITitresimService titService;
        List<Robot> updateRobotList = new List<Robot>();
        List<Motor> updateMotor1List = new List<Motor>();
        List<Motor> updateMotor2List = new List<Motor>();
        List<Ses> updateSesList1 = new List<Ses>();
        List<Ses> updateSesList2 = new List<Ses>();
        List<Titresim> updateTitresimList1 = new List<Titresim>();
        List<Titresim> updateTitresimList2 = new List<Titresim>();
        public Data_Form()
        {
            InitializeComponent();
            motorService = InstanceFactory.GetInstance<IMotorService>();
            robotService = InstanceFactory.GetInstance<IRobotService>();
            sesService = InstanceFactory.GetInstance<ISesService>();
            titService = InstanceFactory.GetInstance<ITitresimService>();
        }
        private void fillGrid()
        {
            sesList = sesService.findSesByTestId(Main_Form.selectedTest);
            ses1List = new BindingList<Ses>(sesList.FindAll(p => p.rostime.Last().Equals('1')));
            ses2List = new BindingList<Ses>(sesList.FindAll(p => p.rostime.Last().Equals('2')));

            titList = titService.findTitresimByTestId(Main_Form.selectedTest);
            tit1List = new BindingList<Titresim>(titList.FindAll(p => p.rostime.Last().Equals('1')));
            tit2List = new BindingList<Titresim>(titList.FindAll(p => p.rostime.Last().Equals('2')));

            motorList = motorService.findMotorByTestId(Main_Form.selectedTest);
            motor1List = new BindingList<Motor>(motorList.FindAll(p => p.rostime.Last().Equals('1')));
            motor2List = new BindingList<Motor>(motorList.FindAll(p => p.rostime.Last().Equals('2')));
            robotList = new BindingList<Robot>(robotService.findRobotByTestId(Main_Form.selectedTest));
            IEnumerable<Tuple<Robot, Motor, Motor>> mainList = robotList.Zip(motor1List, (r, m1) => new { r, m1 }).Zip(motor2List, (z1, m2) => Tuple.Create(z1.r, z1.m1, m2));

            foreach (var (item1, item2, item3) in mainList)
            {
                gridGenel.Rows.Add(Main_Form.selectedTest, item1.rostime, item1.bas_acisi, item1.sicaklik, item1.konum_x, item1.konum_y, item1.ckonum_x, item1.ckonum_y, item1.plab_x, item1.plab_y,
                    item2.cekim_akimi, item2.teker_hizi, item2.guc, item2.gerilim, item2.sicaklik, item2.nem,
                    item3.cekim_akimi, item3.teker_hizi, item3.guc, item3.gerilim, item3.sicaklik, item3.nem);
            }

            gridSes1.DataSource = ses1List;
            gridSes2.DataSource = ses2List;
            gridTitresim1.DataSource = tit1List;
            gridTitresim2.DataSource = tit2List;

            gridSes1.Columns[0].ReadOnly = true;
            gridSes1.Columns[1].ReadOnly = true;
            gridSes2.Columns[0].ReadOnly = true;
            gridSes2.Columns[1].ReadOnly = true;
            gridTitresim1.Columns[0].ReadOnly = true;
            gridTitresim1.Columns[1].ReadOnly = true;
            gridTitresim2.Columns[0].ReadOnly = true;
            gridTitresim2.Columns[1].ReadOnly = true;
            gridGenel.Columns[0].ReadOnly = true;
            gridGenel.Columns[1].ReadOnly = true;

            gridSes1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            gridSes1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            gridSes2.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            gridSes2.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            gridTitresim1.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            gridTitresim1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            gridTitresim2.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            gridTitresim2.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

            ((DataGridViewTextBoxColumn)gridSes1.Columns[2]).MaxInputLength = 50000;
            ((DataGridViewTextBoxColumn)gridSes2.Columns[2]).MaxInputLength = 50000;
        }
        private void Data_Form_Load(object sender, EventArgs e)
        {
            this.BeginInvoke((MethodInvoker) delegate {
                MessageBox.Show("Veriler Yükleniyor. Lütfen Bekleyiniz...", "Yükleniyor", MessageBoxButtons.OK, MessageBoxIcon.Information);
                fillGrid();
            });
        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            updateData();
            MessageBox.Show("Güncelleme işlemi tamamlandı.");
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            deleteData();
        }

        private void deleteData()
        {
            try
            {
                if (tabData.SelectedTab.Name == "tabPage1")
                {
                    DataGridViewRow row = gridGenel.SelectedRows[0];
                    Robot robot = new Robot();
                    Motor motor1 = new Motor();
                    Motor motor2 = new Motor();
                    robot.testId = row.Cells[0].Value.ToString();
                    robot.rostime = row.Cells[1].Value.ToString();
                    robot.bas_acisi = Double.Parse(row.Cells[2].Value.ToString());
                    robot.sicaklik = Double.Parse(row.Cells[3].Value.ToString());
                    robot.konum_x = Double.Parse(row.Cells[4].Value.ToString());
                    robot.konum_y = Double.Parse(row.Cells[5].Value.ToString());
                    robot.ckonum_x = Double.Parse(row.Cells[6].Value.ToString());
                    robot.ckonum_y = Double.Parse(row.Cells[7].Value.ToString());
                    robot.plab_x = Double.Parse(row.Cells[8].Value.ToString());
                    robot.plab_y = Double.Parse(row.Cells[9].Value.ToString());

                    motor1.testId = row.Cells[0].Value.ToString();
                    motor1.rostime = row.Cells[1].Value.ToString();
                    motor1.cekim_akimi = Double.Parse(row.Cells[10].Value.ToString());
                    motor1.teker_hizi = Double.Parse(row.Cells[11].Value.ToString());
                    motor1.guc = Double.Parse(row.Cells[12].Value.ToString());
                    motor1.gerilim = Double.Parse(row.Cells[13].Value.ToString());
                    motor1.sicaklik = Double.Parse(row.Cells[14].Value.ToString());
                    motor1.nem = Double.Parse(row.Cells[15].Value.ToString());

                    motor2.testId = row.Cells[0].Value.ToString();
                    motor2.rostime = row.Cells[1].Value.ToString();
                    motor2.cekim_akimi = Double.Parse(row.Cells[16].Value.ToString());
                    motor2.teker_hizi = Double.Parse(row.Cells[17].Value.ToString());
                    motor2.guc = Double.Parse(row.Cells[18].Value.ToString());
                    motor2.gerilim = Double.Parse(row.Cells[19].Value.ToString());
                    motor2.sicaklik = Double.Parse(row.Cells[20].Value.ToString());
                    motor2.nem = Double.Parse(row.Cells[21].Value.ToString());
                    robotService.deleteData(robot);
                    motorService.deleteData(motor1);
                    motorService.deleteData(motor2);
                    gridGenel.Rows.RemoveAt(row.Index);
                }
                if (tabData.SelectedTab.Name == "tabPage2")
                {
                    Ses ses = gridSes1.CurrentRow.DataBoundItem as Ses;
                    sesService.deleteData(ses);
                    ses1List.Remove(ses);
                    gridSes1.Refresh();
                }
                if (tabData.SelectedTab.Name == "tabPage3")
                {
                    Ses ses = gridSes2.CurrentRow.DataBoundItem as Ses;
                    sesService.deleteData(ses);
                    ses2List.Remove(ses);
                    gridSes2.Refresh();
                }
                if (tabData.SelectedTab.Name == "tabPage4")
                {
                    Titresim tit = gridTitresim1.CurrentRow.DataBoundItem as Titresim;
                    titService.deleteData(tit);
                    tit1List.Remove(tit);
                    gridTitresim1.Refresh();
                }
                if (tabData.SelectedTab.Name == "tabPage5")
                {
                    Titresim tit = gridTitresim2.CurrentRow.DataBoundItem as Titresim;
                    titService.deleteData(tit);
                    tit2List.Remove(tit);
                    gridTitresim2.Refresh();
                }
                MessageBox.Show("Veri başarıyla silindi");
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                MessageBox.Show("Veri silinemedi. Lütfen doğru veriyi seçtiğinizden emin olun.");
            }
        }

        private void updateData()
        {
            if (updateRobotList.Count != 0)
            {
                robotService.updateData(updateRobotList);
            }
            if (updateMotor1List.Count != 0)
            {
                motorService.updateData(updateMotor1List);
            }
            if (updateMotor2List.Count != 0)
            {
                motorService.updateData(updateMotor2List);
            }
            if (updateSesList1.Count != 0)
            {
                sesService.updateData(updateSesList1);
            }
            if (updateSesList2.Count != 0)
            {
                sesService.updateData(updateSesList2);
            }
            if (updateTitresimList1.Count != 0)
            {
                titService.updateData(updateTitresimList1);
            }
            if (updateTitresimList2.Count != 0)
            {
                titService.updateData(updateTitresimList2);
            }
        }
        private void gridGenel_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Robot robot = new Robot();
                Motor motor1 = new Motor();
                Motor motor2 = new Motor();
                Console.WriteLine(e.RowIndex);
                robot.testId = gridGenel.Rows[e.RowIndex].Cells[0].Value.ToString();
                robot.rostime = gridGenel.Rows[e.RowIndex].Cells[1].Value.ToString();
                robot.bas_acisi = Double.Parse(gridGenel.Rows[e.RowIndex].Cells[2].Value.ToString());
                robot.sicaklik = Double.Parse(gridGenel.Rows[e.RowIndex].Cells[3].Value.ToString());
                robot.konum_x = Double.Parse(gridGenel.Rows[e.RowIndex].Cells[4].Value.ToString());
                robot.konum_y = Double.Parse(gridGenel.Rows[e.RowIndex].Cells[5].Value.ToString());
                robot.ckonum_x = Double.Parse(gridGenel.Rows[e.RowIndex].Cells[6].Value.ToString());
                robot.ckonum_y = Double.Parse(gridGenel.Rows[e.RowIndex].Cells[7].Value.ToString());
                robot.plab_x = Double.Parse(gridGenel.Rows[e.RowIndex].Cells[8].Value.ToString());
                robot.plab_y = Double.Parse(gridGenel.Rows[e.RowIndex].Cells[9].Value.ToString());

                motor1.testId = gridGenel.Rows[e.RowIndex].Cells[0].Value.ToString();
                motor1.rostime = gridGenel.Rows[e.RowIndex].Cells[1].Value.ToString();
                motor1.cekim_akimi = Double.Parse(gridGenel.Rows[e.RowIndex].Cells[10].Value.ToString());
                motor1.teker_hizi = Double.Parse(gridGenel.Rows[e.RowIndex].Cells[11].Value.ToString());
                motor1.guc = Double.Parse(gridGenel.Rows[e.RowIndex].Cells[12].Value.ToString());
                motor1.gerilim = Double.Parse(gridGenel.Rows[e.RowIndex].Cells[13].Value.ToString());
                motor1.sicaklik = Double.Parse(gridGenel.Rows[e.RowIndex].Cells[14].Value.ToString());
                motor1.nem = Double.Parse(gridGenel.Rows[e.RowIndex].Cells[15].Value.ToString());

                motor2.testId = gridGenel.Rows[e.RowIndex].Cells[0].Value.ToString();
                motor2.rostime = gridGenel.Rows[e.RowIndex].Cells[1].Value.ToString();
                motor2.cekim_akimi = Double.Parse(gridGenel.Rows[e.RowIndex].Cells[16].Value.ToString());
                motor2.teker_hizi = Double.Parse(gridGenel.Rows[e.RowIndex].Cells[17].Value.ToString());
                motor2.guc = Double.Parse(gridGenel.Rows[e.RowIndex].Cells[18].Value.ToString());
                motor2.gerilim = Double.Parse(gridGenel.Rows[e.RowIndex].Cells[19].Value.ToString());
                motor2.sicaklik = Double.Parse(gridGenel.Rows[e.RowIndex].Cells[20].Value.ToString());
                motor2.nem = Double.Parse(gridGenel.Rows[e.RowIndex].Cells[21].Value.ToString());

                updateMotor1List.Add(motor1);
                updateMotor2List.Add(motor2);
                updateRobotList.Add(robot);
            }
        }
        private void gridSes1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            Ses ses = new Ses();
            ses.testId = gridSes1.Rows[e.RowIndex].Cells[0].Value.ToString();
            ses.rostime = gridSes1.Rows[e.RowIndex].Cells[1].Value.ToString();
            ses.ses_data = gridSes1.Rows[e.RowIndex].Cells[2].Value.ToString();
            updateSesList1.Add(ses);
        }
        private void gridSes2_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            Ses ses = new Ses();
            ses.testId = gridSes2.Rows[e.RowIndex].Cells[0].Value.ToString();
            ses.rostime = gridSes2.Rows[e.RowIndex].Cells[1].Value.ToString();
            ses.ses_data = gridSes2.Rows[e.RowIndex].Cells[2].Value.ToString();
            updateSesList2.Add(ses);
        }
        private void gridTitresim1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            Titresim titresim = new Titresim();
            titresim.testId = gridTitresim1.Rows[e.RowIndex].Cells[0].Value.ToString();
            titresim.rostime = gridTitresim1.Rows[e.RowIndex].Cells[1].Value.ToString();
            titresim.ivme_x = gridTitresim1.Rows[e.RowIndex].Cells[2].Value.ToString();
            titresim.ivme_y = gridTitresim1.Rows[e.RowIndex].Cells[3].Value.ToString();
            titresim.ivme_z = gridTitresim1.Rows[e.RowIndex].Cells[4].Value.ToString();
            updateTitresimList1.Add(titresim);
        }
        private void gridTitresim2_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            Titresim titresim = new Titresim();
            titresim.testId = gridTitresim2.Rows[e.RowIndex].Cells[0].Value.ToString();
            titresim.rostime = gridTitresim2.Rows[e.RowIndex].Cells[1].Value.ToString();
            titresim.ivme_x = gridTitresim2.Rows[e.RowIndex].Cells[2].Value.ToString();
            titresim.ivme_y = gridTitresim2.Rows[e.RowIndex].Cells[3].Value.ToString();
            titresim.ivme_z = gridTitresim2.Rows[e.RowIndex].Cells[4].Value.ToString();
            updateTitresimList2.Add(titresim);
        }
        private void Data_Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (MessageBox.Show("Emin misiniz?", "Çıkış", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                    this.Hide();
            }
        }
    }
}
