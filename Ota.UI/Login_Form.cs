﻿using Ota.Entities.Concrete;
using Ota.Service.Abstract;
using Ota.Service.DependencyManager;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ota.UI
{
    public partial class Login_Form : Form
    {
        private IUserService userService;
        public static string userName;
        public static string lastName;
        public static int userId;
        public Login_Form()
        {
            InitializeComponent();
            panel11.Visible = true;
            panel4.Visible = false;
            txtMail1.Select();
            userService = InstanceFactory.GetInstance<IUserService>();
        }
        private void Login_Failed(string message)
        {
            txtPassword.Text = string.Empty;
            MessageBox.Show(message, "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        private void Login_Success()
        {
            Form menu = new Main_Form();
            menu.Show();
            Hide();
        }
        private void btnSignUp1_Click(object sender, EventArgs e)
        {
            foreach (var item in panel1.Controls)
            {
                if (item is TextBox txt)
                {
                    txt.BackColor = Color.White;
                }
                if(item is MaskedTextBox msk)
                {
                    msk.BackColor = Color.White;
                }
            }
            if (!txtMail.Text.Equals(String.Empty) && !txtPassword.Text.Equals(String.Empty) && !txtFirstName.Text.Equals(String.Empty) && !txtLastName.Text.Equals(String.Empty) 
                && txtBirthday.MaskCompleted)
            {
                try
                {
                    User user = new User();
                    user.isim = txtFirstName.Text;
                    user.soyisim = txtLastName.Text;
                    user.mail = txtMail.Text;
                    user.sifre = txtPassword.Text;
                    String[] dogum = txtBirthday.Text.Split('.');
                    int gun = Int32.Parse(dogum[0]);
                    int ay = Int32.Parse(dogum[1]);
                    int yil = Int32.Parse(dogum[2]);
                    user.Kl_dogum_tarihi = new DateTime(yil, ay, gun);
                    userService.AddUser(user);
                    sendMail(txtMail.Text);
                    if (MessageBox.Show("Kayıt işlemi başarılı şekilde tamamlandı.", "Başarılı", MessageBoxButtons.OK, MessageBoxIcon.Information) == DialogResult.OK)
                    {
                        foreach (var item in panel1.Controls)
                        {
                            if (item is TextBox txt)
                            {
                                txt.Clear();
                            }
                            if (item is MaskedTextBox msk)
                            {
                                msk.Clear();
                            }
                        }
                    }
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message);
                }
            }
            else
            {
                foreach (var item in panel1.Controls)
                {
                    if (item is TextBox txt && txt.Text == String.Empty)
                    {
                        txt.BackColor = Color.Red;
                    }
                    if (item is MaskedTextBox msk && !msk.MaskCompleted)
                    {
                        msk.BackColor = Color.Red;
                    }
                }
            }
        }

        private void sendMail(String email)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
                mail.From = new MailAddress("otareportysystem@gmail.com");
                mail.To.Add(email);
                mail.Subject = "Registration Successful";
                mail.Body = "Your registration has been successfully completed";
                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("otareportysystem", "sdemir2626");
                SmtpServer.EnableSsl = true;
                string userToken = "Success";
                SmtpServer.SendAsync(mail, userToken);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnSignIn1_Click(object sender, EventArgs e)
        {
            try
            {
                User user = userService.GetUser(txtMail1.Text, txtPassword1.Text);
                userName = user.isim;
                lastName = user.soyisim;
                userId = user.id;
                Login_Success();
            }
            catch (Exception ex)
            {
                Login_Failed(ex.Message);
                txtPassword1.Clear();
            }
        }
        private void btnSignUp_Click(object sender, EventArgs e)
        {
            panel4.Visible = true;
            btnSignUp.ForeColor = Color.White;
            btnSignIn.ForeColor = Color.Black;
            if (panel1.Visible == false)
            {
                panel4.Left = btnSignUp.Left;
                btnSignUp.BackColor = Color.FromArgb(26, 177, 136);
                btnSignIn.BackColor = Color.White;

                panel11.Visible = false;
                panel2.Visible = false;
                panel1.Visible = true;
                panel1.Refresh();

            }
        }
        private void btnSignIn_Click(object sender, EventArgs e)
        {
            panel4.Visible = true;
            btnSignUp.ForeColor = Color.Black;
            btnSignIn.ForeColor = Color.White;
            if (panel2.Visible == false)
            {
                panel4.Left = btnSignIn.Left;
                btnSignIn.BackColor = Color.FromArgb(26, 177, 136);
                btnSignUp.BackColor = Color.White;

                panel11.Visible = false;
                panel1.Visible = false;
                panel2.Visible = true;
                panel2.Refresh();

            }
        }
        private void txtPassword1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSignIn1_Click(this, new EventArgs());
            }
        }
        private void txt_Click(object sender, EventArgs e)
        {
            String controlName = ((Control)sender).Name;
            switch (controlName)
            {
                case "txtFirstName": changeColorOfControl(panel7, 1); break;
                case "txtLastName": changeColorOfControl(panel8, 1); break;
                case "txtBirthday": changeColorOfControl(panel6, 1); break;
                case "txtMail": changeColorOfControl(panel5, 1); break;
                case "txtPassword": changeColorOfControl(panel3, 1); break;
                case "txtMail1": changeColorOfControl(panel9, 1); break;
                case "txtPassword1": changeColorOfControl(panel10, 1); break;

            }
        }
        private void txt_Leave(object sender, EventArgs e)
        {
            String controlName = ((Control)sender).Name;
            switch (controlName)
            {
                case "txtFirstName": changeColorOfControl(panel7, 2); break;
                case "txtLastName": changeColorOfControl(panel8, 2); break;
                case "txtBirthday": changeColorOfControl(panel6, 2); break;
                case "txtMail": changeColorOfControl(panel5, 2); break;
                case "txtPassword": changeColorOfControl(panel3, 2); break;
                case "txtMail1": changeColorOfControl(panel9, 2); break;
                case "txtPassword1": changeColorOfControl(panel10, 2); break;

            }
        }
        private void changeColorOfControl(Control control, int colorTur)
        {
            if (colorTur == 1)
            {
                control.BackColor = Color.FromArgb(26, 177, 136);
            }
            else
            {
                control.BackColor = Color.White;
            }
        }
        private void Login_Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (MessageBox.Show("Emin misiniz?", "Çıkış", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
                    e.Cancel = true;
                else
                    Application.Exit();
            }
        }
    }
}
