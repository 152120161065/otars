﻿using Ota.Data.Abstract;
using Ota.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Ota.Data.Concrete.EntityFramework
{
    public class EfEntityRepositoryBase<TEntity, TContext> : IEntityRepository<TEntity>
        where TEntity : class, IEntity, new()
        where TContext : DbContext, new()
    {
        public TEntity Get(Expression<Func<TEntity, bool>> filter)
        {
            using (TContext context = new TContext())
            {
                return context.Set<TEntity>().FirstOrDefault(filter);
            }
        }
        public void Add(List<TEntity> entityList)
        {
            using (TContext context = new TContext())
            {
                context.Set<TEntity>().AddRange(entityList);
                context.SaveChanges();
            }
        }
        public List<TEntity> GetByTestId(Expression<Func<TEntity, bool>> filter)
        {
            using (TContext context = new TContext())
            {
                return context.Set<TEntity>().Where(filter).ToList();
            }
        }
        public List<TEntity> GetAll()
        {
            using (TContext context = new TContext())
            {
                return context.Set<TEntity>().ToList();
            }
        }
        public void AddSingle(TEntity entity)
        {
            using (TContext context = new TContext())
            {
                context.Set<TEntity>().Add(entity);
                context.SaveChanges();
            }
        }
        public void UpdateData(List<TEntity> updateData)
        {
            using (TContext context = new TContext())
            {
                foreach (var entity in updateData)
                {
                    context.Entry(entity).State = EntityState.Modified;
                }
                try
                {
                    context.SaveChanges();
                }
                catch(Exception ex)
                {
                    //Log4Net can be use later
                    Console.WriteLine(ex.Message);
                }
            }
        }
        public void DeleteData(TEntity entity)
        {
            using (TContext context = new TContext())
            {
                context.Entry(entity).State = EntityState.Deleted;
                try
                {
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    //Log4Net can be use later
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
