﻿using Ota.Data.Abstract;
using Ota.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ota.Data.Concrete.EntityFramework
{
    public class EfSesDal : EfEntityRepositoryBase<Ses, OtaContext>, ISesDal
    {
    }
}
