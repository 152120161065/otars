﻿using Ota.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ota.Data.Concrete.EntityFramework
{
    public class OtaContext : DbContext
    {
        public DbSet<User> Tbl_Kullanici { get; set; }
        public DbSet<Motor> Tbl_Motor { get; set; }
        public DbSet<Robot> Tbl_Robot { get; set; }
        public DbSet<Ses> Tbl_Ses { get; set; }
        public DbSet<Titresim> Tbl_Titresim { get; set; }
        public DbSet<Test> Tbl_Test { get; set; }
    }
}
