﻿using Ota.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ota.Data.Abstract
{
    public interface IUserDal : IEntityRepository<User>
    {
    }
}
