﻿using Ota.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Ota.Data.Abstract
{
    public interface IEntityRepository<T> where T : class, IEntity, new()
    {
        T Get(Expression<Func<T, bool>> filter);
        void Add(List<T> entityList);
        List<T> GetByTestId(Expression<Func<T, bool>> filter);
        List<T> GetAll();
        void AddSingle(T entity);
        void UpdateData(List<T> entityList);
        void DeleteData(T entity);
    }
}
