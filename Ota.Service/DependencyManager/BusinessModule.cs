﻿using Ninject.Modules;
using Ota.Data.Abstract;
using Ota.Data.Concrete.EntityFramework;
using Ota.Service.Abstract;
using Ota.Service.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ota.Service.DependencyManager
{
    public class BusinessModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUserService>().To<UserManager>().InSingletonScope();
            Bind<IUserDal>().To<EfUserDal>().InSingletonScope();

            Bind<IMotorService>().To<MotorManager>().InSingletonScope();
            Bind<IMotorDal>().To<EfMotorDal>().InSingletonScope();

            Bind<IRobotService>().To<RobotManager>().InSingletonScope();
            Bind<IRobotDal>().To<EfRobotDal>().InSingletonScope();

            Bind<ISesService>().To<SesManager>().InSingletonScope();
            Bind<ISesDal>().To<EfSesDal>().InSingletonScope();

            Bind<ITitresimService>().To<TitresimManager>().InSingletonScope();
            Bind<ITitresimDal>().To<EfTitresimDal>().InSingletonScope();

            Bind<ITestService>().To<TestManager>().InSingletonScope();
            Bind<ITestDal>().To<EfTestDal>().InSingletonScope();
        }
    }
}
