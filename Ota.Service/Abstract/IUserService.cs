﻿using Ota.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ota.Service.Abstract
{
    public interface IUserService
    {
        User GetUser(string email, string password);
        void AddUser(User user);
    }
}
