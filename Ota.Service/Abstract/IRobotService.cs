﻿using Ota.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ota.Service.Abstract
{
    public interface IRobotService
    {
        void addRobot(List<Robot> robot);
        List<Robot> findRobotByTestId(String testId);
        void updateData(List<Robot> robotList);
        void deleteData(Robot robot);
    }
}
