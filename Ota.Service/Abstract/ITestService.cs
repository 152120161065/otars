﻿using Ota.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ota.Service.Abstract
{
    public interface ITestService
    {
        void addTest(Test test);
        List<Test> GetAll();
    }
}
