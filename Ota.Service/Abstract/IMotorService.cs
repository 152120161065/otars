﻿using Ota.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ota.Service.Abstract
{
    public interface IMotorService
    {
        void addMotor(List<Motor> motor);
        List<Motor> findMotorByTestId(String testId);
        void updateData(List<Motor> motorList);
        void deleteData(Motor motor);
    }
}
