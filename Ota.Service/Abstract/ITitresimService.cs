﻿using Ota.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ota.Service.Abstract
{
    public interface ITitresimService
    {
        void addTitresim(List<Titresim> titresim);
        List<Titresim> findTitresimByTestId(String testId);
        void updateData(List<Titresim> titresimList);
        void deleteData(Titresim titresim);
    }
}
