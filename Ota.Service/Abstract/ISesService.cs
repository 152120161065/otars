﻿using Ota.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ota.Service.Abstract
{
    public interface ISesService
    {
        void addSes(List<Ses> ses);
        List<Ses> findSesByTestId(String testId);
        void updateData(List<Ses> sesList);
        void deleteData(Ses ses);
    }
}
