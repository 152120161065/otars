﻿using Ota.Data.Abstract;
using Ota.Entities.Concrete;
using Ota.Service.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ota.Service.Concrete
{
    public  class SesManager : ISesService
    {
        private ISesDal _sesDal;
        public SesManager(ISesDal sesDal)
        {
            _sesDal = sesDal;
        }
        public void addSes(List<Ses> ses)
        {
            _sesDal.Add(ses);
        }

        public void deleteData(Ses ses)
        {
            _sesDal.DeleteData(ses);
        }

        public List<Ses> findSesByTestId(String testId)
        {
            List<Ses> sesList = _sesDal.GetByTestId(p => p.testId.Equals(testId));
            if (sesList.Count == 0)
            {
                throw new Exception("Bu teste ait veri bulunamadı!");
            }
            return sesList;
        }
        public void updateData(List<Ses> sesList)
        {
            _sesDal.UpdateData(sesList);
        }
    }
}
