﻿using Ota.Data.Abstract;
using Ota.Entities.Concrete;
using Ota.Service.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ota.Service.Concrete
{
    public class TestManager : ITestService
    {
        private ITestDal _testDal;
        public TestManager(ITestDal testDal)
        {
            _testDal = testDal;
        }
        public List<Test> GetAll()
        {
            List<Test> testList = _testDal.GetAll();
            if (testList.Count == 0)
            {
                throw new Exception("Test verisi bulunamadı!");
            }
            return testList;
        }
        public void addTest(Test test)
        {
            _testDal.AddSingle(test);
        }
    }
}
