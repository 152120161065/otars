﻿using Ota.Data.Abstract;
using Ota.Entities.Concrete;
using Ota.Service.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ota.Service.Concrete
{
    public class RobotManager : IRobotService
    {
        private IRobotDal _robotDal;
        public RobotManager(IRobotDal robotDal)
        {
            _robotDal = robotDal;
        }
        public void addRobot(List<Robot> robot)
        {
            _robotDal.Add(robot);
        }
        public void deleteData(Robot robot)
        {
            _robotDal.DeleteData(robot);
        }
        public List<Robot> findRobotByTestId(String testId)
        {
            List<Robot> robotList = _robotDal.GetByTestId(p => p.testId.Equals(testId));
            if (robotList.Count == 0)
            {
                throw new Exception("Bu teste ait veri bulunamadı!");
            }
            return robotList;
        }
        public void updateData(List<Robot> robotList)
        {
            _robotDal.UpdateData(robotList);
        }
    }
}
