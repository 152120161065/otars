﻿using Ota.Data.Abstract;
using Ota.Entities.Concrete;
using Ota.Service.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ota.Service.Concrete
{
    public class MotorManager : IMotorService
    {
        private IMotorDal _motorDal;
        public MotorManager(IMotorDal motorDal)
        {
            _motorDal = motorDal;
        }
        public void addMotor(List<Motor> motor)
        {
            _motorDal.Add(motor);
        }

        public List<Motor> findMotorByTestId(String testId)
        {
            List<Motor> motorList = _motorDal.GetByTestId(p => p.testId.Equals(testId));
            if (motorList.Count == 0)
            {
                throw new Exception("Bu teste ait veri bulunamadı!");
            }
            return motorList;
        }
        public void updateData(List<Motor> motorList)
        {
            _motorDal.UpdateData(motorList);
        }
        public void deleteData(Motor motor)
        {
            _motorDal.DeleteData(motor);
        }
    }
}
