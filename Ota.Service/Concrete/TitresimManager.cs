﻿using Ota.Data.Abstract;
using Ota.Entities.Concrete;
using Ota.Service.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ota.Service.Concrete
{
    public class TitresimManager : ITitresimService
    {
        private ITitresimDal _titresimDal;
        public TitresimManager(ITitresimDal titresimDal)
        {
            _titresimDal = titresimDal;
        }
        public void addTitresim(List<Titresim> titresim)
        {
            _titresimDal.Add(titresim);
        }

        public void deleteData(Titresim titresim)
        {
            _titresimDal.DeleteData(titresim);
        }

        public List<Titresim> findTitresimByTestId(String testId)
        {
            List<Titresim> titresimList = _titresimDal.GetByTestId(p => p.testId.Equals(testId));
            if (titresimList.Count == 0)
            {
                throw new Exception("Bu teste ait veri bulunamadı!");
            }
            return titresimList;
        }
        public void updateData(List<Titresim> titresimList)
        {
            _titresimDal.UpdateData(titresimList);
        }
    }
}
