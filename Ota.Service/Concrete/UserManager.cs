﻿using Ota.Data.Abstract;
using Ota.Entities.Concrete;
using Ota.Service.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ota.Service.Concrete
{
    public class UserManager : IUserService
    {
        private IUserDal _userDal;
        public UserManager(IUserDal userDal)
        {
            _userDal = userDal;
        }
        public User GetUser(string email, string password)
        {
            User user = _userDal.Get(p => p.mail.Equals(email) && p.sifre.Equals(password));
            if (user == null)
            {
                throw new Exception("Hatalı kullanıcı adı veya şifre");
            }
            return user;
        }
        public void AddUser(User user)
        {
            _userDal.AddSingle(user);
        }
    }
}
